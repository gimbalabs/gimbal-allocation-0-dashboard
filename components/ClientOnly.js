// https://blog.logrocket.com/why-use-next-js-apollo/#fetching-data-apollo-client-ssr-pages
// originally written by Josh Comeau

// Now try to implement one of the A0 tables, as a component.
// It will be wrapped in this ClientOnly component.

import { useEffect, useState } from "react";

export default function ClientOnly({ children, ...delegated }) {
  const [hasMounted, setHasMounted] = useState(false);

  useEffect(() => {
    setHasMounted(true);
  }, []);

  if (!hasMounted) {
    return null;
  }

  return <div {...delegated}>{children}</div>;
}