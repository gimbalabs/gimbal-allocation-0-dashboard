import { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import Link from 'next/link';

import { Flex, Center, Heading, Text, Box, List, ListItem, OrderedList, UnorderedList } from "@chakra-ui/react";

// WITH VARIABLE:
const TXHASH_QUERY = gql`
  query AddressFromUtxo($txhash: Hash32Hex!) {
    transactions(where: { hash: { _eq: $txhash } }) {
      outputs {
        address
      }
    }
  }
`;


export default function AZeroAddressList() {
    const allocationTxHash = "ac90e28c7f3a10e6c24641fbe0d74c02bcd00008a350abf2a27f15d2f5d4a8e0"

    const [addresses, setAddresses] = useState(null)

    const { data, loading, error } = useQuery(TXHASH_QUERY, {
        variables: {
            txhash: allocationTxHash
        }
    });

    useEffect(() => {
        let result = []
        if (transactions?.length > 0) {
            transactions[0].outputs?.map(output => {
                result.push(output.address)
            })
            setAddresses(result)
        }
    }, [transactions, loading])

    if (loading) {
        return (
            <Heading size="lg">Loading data...</Heading>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };

    const { transactions } = data;


    return (
        <Box w="50%" mx="auto" my='10' p='5' bg="green">
            <Heading size="lg">Allocation 0 was distributed on 2022-04-12 to:</Heading>
            <Heading size="md" py='2'>
                <Link href="https://cardanoscan.io/transaction/ac90e28c7f3a10e6c24641fbe0d74c02bcd00008a350abf2a27f15d2f5d4a8e0">View transaction on Cardanoscan</Link>
            </Heading>
            {addresses?.map(address => <Text py='2' fontSize="xs" key={address}>{address}</Text>)}
            <Box w="90%" mx="auto" my='3' p='3' bg="red">
                Note: the data in this box comes from an on-chain query, but it does not have to. This list of addresses is provided so that anyone can verify that the addresses hard-coded into the table below match the allocation transaction.
            </Box>
        </Box>
    )
}