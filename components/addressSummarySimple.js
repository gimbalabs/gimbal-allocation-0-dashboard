import { useQuery, gql } from "@apollo/client";
import { Box, Heading, Text } from "@chakra-ui/react";

// WITH VARIABLE:
// const QUERY = gql`
//     query AddressesBalances($addr : [String]!){
//         paymentAddresses(addresses: $addr) {
//             address
//             summary {
//                 assetBalances {
//                     asset {
//                         policyId
//                     }
//                     quantity
//                 }
//             }
//         }
//     }
// `;

// WITHOUT VARIABLE:
const QUERY = gql`
    query AddressesBalances{
        paymentAddresses(addresses: ["addr1qx2h42hnke3hf8n05m2hzdaamup6edfqvvs2snqhmufv0eryqhtfq6cfwktmrdw79n2smpdd8n244z8x9f3267g8cz6shnc9au"]) {
            address
            summary {
                assetBalances {
                    asset {
                        policyId
                    }
                    quantity
                }
            }
        }
    }
`;

export default function AddressSummarySimple() {
    const {data, loading, error} = useQuery(QUERY);

    if (loading) {
        return (
            <Heading size="lg">Loading data...</Heading>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };

    const {paymentAddresses} = data;

    return (
        <Box m='5' p='5' bg='pink'>
            <Text>{JSON.stringify(paymentAddresses)}</Text>
        </Box>
    )
}