import { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import Link from 'next/link';

import {
    Flex, Center, Heading, Text, Box,
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
} from "@chakra-ui/react";

import { allocationZero } from "../data/a0";

// WITH VARIABLE:
const QUERY = gql`
    query AddressesBalances($addr : [String]!){
        paymentAddresses(addresses: $addr) {
            address
            summary {
                assetBalances {
                    asset {
                        policyId
                    }
                    quantity
                }
            }
        }
    }
`;


export default function AddressTable() {
    const a0Addresses = Array.from(allocationZero, elem => elem.address)
    const gimbalPolicyId = "2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c30"

    const [tokenBalances, setTokenBalances] = useState([])

    const { data, loading, error } = useQuery(QUERY, {
        variables: {
            addr: a0Addresses
        }
    });

    useEffect(() => {
        let result = [];
        console.log("hello")
        if (paymentAddresses) {
            paymentAddresses.map(wallet => {
                const address = wallet.address;
                let balance = 0;
                const check = wallet.summary.assetBalances.find(bal => bal.asset.policyId == gimbalPolicyId);
                if (check) {
                    balance = check.quantity / 1000000;
                    result.push({ address, balance })
                } else {
                    result.push({ address, balance })
                }
            })
        }
        setTokenBalances(result)
    }, [paymentAddresses, loading])

    if (loading) {
        return (
            <Heading size="lg">Loading data...</Heading>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };

    const { paymentAddresses } = data;


    return (
        <Box w="100%" mx="auto" p="5" bg="blue">
            <Heading p='5'>
                Current Gimbal Balance for each Allocation 0 Working Group:
            </Heading>
            <Text px='5' pb='5'>
                CURRENT GIMBALS data is pulled from a GraphQL query.
            </Text>
            <Box w="90%" p='1' bg="white" mx="auto">
                <TableContainer>
                    <Table variant="striped" size="sm" colorScheme="yellow">
                        <Thead>
                            <Tr>
                                <Th>Working Group - Click to view address on CardanoScan</Th>
                                <Th isNumeric>A0 Gimbals</Th>
                                <Th isNumeric>Current Gimbals</Th>
                                <Th>Working Group Treasury</Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {allocationZero.map(wg => (
                                <Tr key={wg.address}>
                                    <Td><Link href={`a0/${wg.address}`}>{wg.workingGroup}</Link></Td>
                                    <Td isNumeric>{wg.gimbalsA0.toLocaleString()}</Td>
                                    <Td isNumeric>{
                                        tokenBalances.find(wallet => wallet.address == wg.address) ?
                                        tokenBalances.find(wallet => wallet.address == wg.address).balance.toLocaleString() :
                                        wg.gimbalBalance
                                    }</Td>
                                    <Td>
                                        {wg.treasuryLink ? (
                                            <a href={wg.treasuryLink}>View Working Group Treasury</a>
                                        ) : "--"}
                                    </Td>
                                </Tr>
                            ))}
                        </Tbody>
                    </Table>
                </TableContainer>
            </Box>
        </Box>
    )
}