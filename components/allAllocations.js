import React from "react";
import {
    Flex, Center, Heading, Text, Box,
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
} from "@chakra-ui/react";
import { allocations } from "../data/allocations"

export default function AllAllocations() {

    return (
        <Box w="100%" mx="auto" bg="green" p="5" my="10">
            <Heading py='5'>All Gimbal Token Allocations</Heading>
            <Text>This list is hard-coded into this project.</Text>
            <Box w="100%" p='1' bg="white">
                <TableContainer>
                    <Table variant="striped" size="sm" colorScheme="yellow">
                        <Thead>
                            <Tr>
                                <Th>Allocation</Th>
                                <Th>Address</Th>
                                <Th>Gimbals</Th>
                                <Th>Current Balance - TODO</Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {allocations.map(allocation => (
                                <Tr key={allocation.title}>
                                    <Td>{allocation.title}</Td>
                                    <Td>{allocation.currentAddress}</Td>
                                    <Td isNumeric>{allocation.gimbalAllocation.toLocaleString()}</Td>
                                </Tr>
                            ))}
                        </Tbody>
                    </Table>
                </TableContainer>
            </Box>
        </Box>
    )
}
