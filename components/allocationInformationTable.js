import React, { useEffect, useState } from "react";
import {
    Flex, Center, Heading, Text, Box,
    Table,
    Thead,
    Tbody,
    Tfoot,
    Tr,
    Th,
    Td,
    TableCaption,
    TableContainer,
} from "@chakra-ui/react";
import { allocationZero } from "../data/a0"

export default function AllocationInformationTable() {

    return (
        <Box w="100%" mx="auto" bg="green" p="5" my="10">
            <Heading>Initial Working Group Distribution</Heading>
            <Text py='2'>The data displayed in this table is hard-coded into this project. Anyone can verify that the transaction happened by looking for it on chain (see link above), but we might not want to call an API every time we want to review the data.</Text>
            <Box w="100%" p='1' bg="white">
                <TableContainer fontSize='xs'>
                    <Table variant="striped" size="sm" colorScheme="yellow">
                        <Thead>
                            <Tr>
                                <Th>Working Group</Th>
                                <Th>Address</Th>
                                <Th>ADA</Th>
                                <Th>Gimbals</Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {allocationZero.map(workingGroup => (
                                <Tr key={workingGroup.address}>
                                    <Td>{workingGroup.workingGroup}</Td>
                                    <Td>{workingGroup.address}</Td>
                                    <Td isNumeric>{workingGroup.lovelace / 1000000}</Td>
                                    <Td isNumeric>{workingGroup.gimbalsA0.toLocaleString()}</Td>
                                </Tr>
                            ))}
                        </Tbody>
                    </Table>
                </TableContainer>
            </Box>
        </Box>
    )
}