import Link from 'next/link'
import { Flex, Spacer } from '@chakra-ui/react'

export default function Navbar() {
  return (
      <Flex direction="row" w="100%" p="5" bg="blue">
        <Spacer />
        <Link href="/">Home</Link>
        <Spacer />
        <Link href="/transactions">Transactions</Link>
        <Spacer />
        <Link href="/dashboard">A0 Dashboard</Link>
        <Spacer />
        <Link href="/templates">Templates</Link>
        <Spacer />
        <Link href="/wallet">Basic Wallet Interactions</Link>
        <Spacer />
      </Flex>
  )
}