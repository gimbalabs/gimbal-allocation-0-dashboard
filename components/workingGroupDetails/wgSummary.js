import { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import Link from 'next/link';
import { Box, Heading, Text, Spinner, Center } from '@chakra-ui/react'

import { allocationZero } from "../../data/a0";

const ADDRESS_QUERY = gql`
    query gimbalTransactions($addr: String!) {
        transactions(where : { _and : [
            { inputs : { address : {_eq: $addr } } },
            { outputs: { tokens: { asset: { policyId : { _eq: "2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c30" }}}}}
        ]}) {
            hash
            includedAt
            outputs {
                address
                value
                tokens {
                    asset {
                    policyId
                        assetName
                    }
                    quantity
                }
            }
        }
    }
`;

export default function WorkingGroupSummary(props) {
    const address = props.address;
    const name = allocationZero.find(elem => elem.address === address).workingGroup
    const { data, loading, error } = useQuery(ADDRESS_QUERY, {
        variables: {
            addr: address
        }
    });

    if (loading) {
        return (
            <Center width="100%" m='5' p='5' bg='orange.200'>
                <Spinner size='xl' />
            </Center>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };

    const { transactions } = data;

    return (
        <Box m='5' p='5' bg='orange.200'>
            <Heading>{name}</Heading>
            <Heading size='lg'># Gimbal Txs: {transactions.length}</Heading>
            {transactions.map(transaction => (
                <Text p='2' key={transaction.hash}>{transaction.hash} - {transaction.includedAt} - {JSON.stringify(transaction.outputs)}</Text>
            ))}
        </Box>
    )
}
