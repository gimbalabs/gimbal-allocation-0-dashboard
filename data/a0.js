export const allocationZero = [
    {
        "workingGroup": "Bridge Builders",
        "address": "addr1qy0xtuuml54hl0dfmf5wmen8qsuc9amqgf2pe8kv6ryvgz0sur029s675j25khs4umamx59rdq4phm97xh7jj34uvkcsl8txw6",
        "lovelace": 100000000,
        "gimbalsA0": 923724,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "Cardano 4 Climate",
        "address": "addr1qy7h7vh39sfl3qgjcxdz259zl65p53xjevcypfvjdsg3ghsf4p9cdh7w2axu4t8v22fglzu8h5p3zen6cxe34s6n3ycq7exaa2",
        "lovelace": 100000000,
        "gimbalsA0": 917864,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "Cardano After Dark",
        "address": "addr1q9xh7sh7kvqskayvf5v4ql84kp897aft8m6xn9my67hw58ju6zg5vdk0vvje6yg4ah309l2rkc54cqujdwucpt6807dq5tf4gg",
        "lovelace": 100000000,
        "gimbalsA0": 563334,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "Catalyst Constitution",
        "address": "addr1q9vv4d6me424w7gf4yx55km65u7zpzhrwzy53nuhrkswytm9dnurknrf7yutan07ynz6m7ngftn00fhssfs86ck0lhnsk206e9",
        "lovelace": 100000000,
        "gimbalsA0": 806216,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "Catalyst Proposal Facilitation",
        "address": "addr1q8383mq7c4llmyur5qda4xsvve3jv729p36qmldfgelahtvyfuktlthg0l8s6hc2286zrzj9ttf52nplqy0vwlpda6nqsjytr4",
        "lovelace": 100000000,
        "gimbalsA0": 704798,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "Connectors, Cross Pollinators, Network Builders, Recruiters",
        "address": "addr1qyrgj5c5nglmpjf7dz28hl20mdzdvvmseg3emdeapuf0l2v53prrwwhnntf5rysu6whkaxdk2qzn9s4sc4yxfha8064s4m4qgd",
        "lovelace": 100000000,
        "gimbalsA0": 1023482,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "DEMU Jukebox",
        "address": "addr1q8wdu854r55lfaz220usklzd3sn2spmmdhnhws0n52qhsfqg9a3ps3vt65va0ykpllq089k200d98nukuehhkqakzm4sxcesny",
        "lovelace": 100000000,
        "gimbalsA0": 859179,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "Legal + Regulatory",
        "address": "addr1qxt98xvepds996ffl59t2rt3rvee0fvv65stlxlpvxgmjzw8mr0uw7lvcrp9l6ndlunfvxck2y3jhu99npkazfy7u4yq7dp67r",
        "lovelace": 100000000,
        "gimbalsA0": 932006,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "Littlefish Foundation",
        "address": "addr1q8ztckwae4uqzpekf63fdngka9gqt3ygvmt7jltkfe6sr4zfv04npxajuq9yx8rgkcwkngfu6yzxph90m2pzucuclu4s5tgsgg",
        "lovelace": 100000000,
        "gimbalsA0": 638039,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "PR, Marketing & Communications",
        "address": "addr1q9873hrr6k6z5mffyldw9cg0ucvksaxs3culejk7lve9qr4hrh3glyv4zyrchyykhgyzyyh0yt35q42re4dlzwvuvz4qt33jtj",
        "lovelace": 100000000,
        "gimbalsA0": 1013342,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "Project Based Learning",
        "address": "addr1qxdgudzgf00ghdd8tw5ylylk4t76hsgm9pvr9ce73etppk0c8aursjfdhu7nr3sxujgczt2ndefwfc80pphdafv7fnrq7wk9fz",
        "lovelace": 100000000,
        "gimbalsA0": 1222939,
        "gimbalBalance": 0,
        "treasuryLink": "https://a0pblworkinggroup.gatsbyjs.io/"
    },
    {
        "workingGroup": "Technical",
        "address": "addr1qyk68cp64h49aw4d9cp4yh4k2tved8mnzdwujlt24hc7p6vtuty0dkpw2dwxkh8e5pytmsk0y2h9zt9zemcvcnhr43qsua4wu5",
        "lovelace": 100000000,
        "gimbalsA0": 2285202,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "Tokenomics, Treasury, Liquidity",
        "address": "addr1q9ex4cp329k4qzltrfnk4hrpjls3y0fjv2yrfje5hva7c92x8ele2404072kzx0ksfu5jek0hy6tvr4ljvedz4hmnppqdyrclg",
        "lovelace": 100000000,
        "gimbalsA0": 1146109,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "Transparency Data Management",
        "address": "addr1qxtxfernj4qm6nzp93896t6pwe6upp47zrrfqpevf7kj429yp23l66kc8x5l8er6639dpq3vg285lnek8mxphcralgtqpwwxxc",
        "lovelace": 100000000,
        "gimbalsA0": 799951,
        "gimbalBalance": 0,
        "treasuryLink": null
    },
    {
        "workingGroup": "Reserve for feedback processes",
        "address": "addr1qx2h42hnke3hf8n05m2hzdaamup6edfqvvs2snqhmufv0eryqhtfq6cfwktmrdw79n2smpdd8n244z8x9f3267g8cz6shnc9au",
        "lovelace": 0,
        "gimbalsA0": 3725091,
        "gimbalBalance": 0,
        "treasuryLink": null
    }
]
