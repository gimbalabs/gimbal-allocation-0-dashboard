export const allocations = [
    {
        "title": "Not Released",
        "currentAddress": "addr1qy7a3u4t89m3f7lqkjt0vzppaa9ckna5vdyj7yfhm4rgra8ppmsfzp7mq8rdd3hm6tayq2m2cwfnfqsme0n0un5wxwnsxqkf5z",
        "gimbalAllocation": 1562305898.40
    },
    {
        "title": "Allocation 0",
        "currentAddress": "addr1qx2h42hnke3hf8n05m2hzdaamup6edfqvvs2snqhmufv0eryqhtfq6cfwktmrdw79n2smpdd8n244z8x9f3267g8cz6shnc9au",
        "gimbalAllocation": 21286236.14
    },
    {
        "title": "Community Incentives",
        "currentAddress": "addr1qxhcfs7ssp5wuvehxm7l2s9gpqxj5yc6ezguhl5zmj2mks8rkguk9nh40nys7rdnquvcsccxu93q7d76ajjxnnn02pvqdlvyam",
        "gimbalAllocation": 13155617.41
    },
    {
        "title": "DripDropz",
        "currentAddress": "addr1w96sfxpp02q9x0u2s983d2jp28k0evzr4sm22ump368jk0gxwwkpz",
        "gimbalAllocation": 8130618.69
    },
    {
        "title": "Staking Experiments (not released)",
        "currentAddress": null,
        "gimbalAllocation": 5024998.70
    },
    {
        "title": "2021 Core Team Allocation",
        "currentAddress": null,
        "gimbalAllocation": 5024998.70
    },
    {
        "title": "Founder Allocation",
        "currentAddress": null,
        "gimbalAllocation": 3105619.97
    },
]
