/** @type {import('next').NextConfig} */

// https://webpack.js.org/configuration/experiments/
// https://nextjs.org/docs/api-reference/next.config.js/custom-webpack-config

const nextConfig = {
  reactStrictMode: true,
  // webpack: (
  //   config,
  //   { buildId, dev, isServer, defaultLoaders, nextRuntime, webpack }
  // ) => {
  //   config.module.rules.push({
  //     use: [
  //       defaultLoaders.babel,
  //     ],
  //     type: "webassembly/async"
  //   })
  //   // Important: return the modified config
  //   return config
  // },
}

module.exports = nextConfig
