// pages/_app.js
import { ApolloProvider } from "@apollo/client";
import client from "../apollo-client";
import { ChakraProvider, extendTheme } from '@chakra-ui/react'
import Navbar from '../components/navbar'

// ApolloProvider is added for use with Client Side Rendering
// See: https://blog.logrocket.com/why-use-next-js-apollo/#fetching-data-apollo-client-ssr-pages

const colors = {
  'blue': '#738290',
  'green': '#C2D8B9',
  'red': '#E4F0D0'
}

const theme = extendTheme({ colors })

function MyApp({ Component, pageProps }) {
  return (
    <ApolloProvider client={client}>
      <ChakraProvider theme={theme}>
        <Navbar />
        <Component {...pageProps} />
      </ChakraProvider>
    </ApolloProvider>
  )
}

export default MyApp