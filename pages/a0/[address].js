import { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import Link from 'next/link';
import { useRouter } from 'next/router'
import { Box } from '@chakra-ui/react'
import ClientOnly from "../../components/ClientOnly";
import WorkingGroupSummary from "../../components/workingGroupDetails/wgSummary";



// WITHOUT PROPS
export default function AddressDetailsPage() {
  const router = useRouter()
  const { address } = router.query

  return (
    <Box m='5' p='5' bg='#aaaaaa'>
        <p>Address: {address}</p>
        <a href={`https://cardanoscan.io/address/${address}`}>{address} on Cardanoscan</a>
        <ClientOnly>
          <WorkingGroupSummary address={address} />
        </ClientOnly>
    </Box>
  )
}
