import { Box, Heading } from '@chakra-ui/react'
import Head from 'next/head'
import Image from 'next/image'
import ClientOnly from "../components/ClientOnly";


import AllocationInformationTable from '../components/allocationInformationTable'
import AddressTable from '../components/addressTable';
import AllAllocations from '../components/allAllocations';
import AZeroAddressList from '../components/aZeroAddressList';

export default function Dashboard() {
    return (
        <>
            <Head>
                <title>gimbal a0</title>
                <meta name="description" content="Generated by create next app" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Box m='5' p='5'>
                <Heading>A0 Dashboard</Heading>
            </Box>
            <ClientOnly>
                <AddressTable />
            </ClientOnly>
            <ClientOnly>
                <AZeroAddressList />
            </ClientOnly>
            <AllocationInformationTable />
            <AllAllocations />
        </>
    )
}
