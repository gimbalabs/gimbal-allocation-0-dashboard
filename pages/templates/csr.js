import Head from "next/head";
import ClientOnly from "../../components/ClientOnly";
import AddressSummarySimple from "../../components/addressSummarySimple";
import { Box, Heading } from "@chakra-ui/react";

export default function ClientSide() {
  return (
    <Box>
      <Head>
        <title>gimbal a0</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box>
        <Heading>A Simple Example of Client Side Rendering</Heading>
        <ClientOnly>
          <AddressSummarySimple />
        </ClientOnly>
      </Box>
    </Box>
  );
}